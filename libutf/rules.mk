# Copyright 2016 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

MODULE_TYPE := userlib

MODULE_SRCS += \
    $(LOCAL_DIR)/fgetrune.c \
    $(LOCAL_DIR)/fputrune.c \
    $(LOCAL_DIR)/isalnumrune.c \
    $(LOCAL_DIR)/isalpharune.c \
    $(LOCAL_DIR)/isblankrune.c \
    $(LOCAL_DIR)/iscntrlrune.c \
    $(LOCAL_DIR)/isdigitrune.c \
    $(LOCAL_DIR)/isgraphrune.c \
    $(LOCAL_DIR)/isprintrune.c \
    $(LOCAL_DIR)/ispunctrune.c \
    $(LOCAL_DIR)/isspacerune.c \
    $(LOCAL_DIR)/istitlerune.c \
    $(LOCAL_DIR)/isxdigitrune.c \
    $(LOCAL_DIR)/lowerrune.c \
    $(LOCAL_DIR)/rune.c \
    $(LOCAL_DIR)/runetype.c \
    $(LOCAL_DIR)/runetype.h \
    $(LOCAL_DIR)/upperrune.c \
    $(LOCAL_DIR)/utf.c \
    $(LOCAL_DIR)/utftorunestr.c \

MODULE_LIBS += system/ulib/c

include make/module.mk
