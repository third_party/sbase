# Copyright 2017 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

THIRD_PARTY_SBASE_DIR := $(GET_LOCAL_DIR)

# These do not yet build
SBASE_NOT_WORKING := \
    chroot \
    getconf \
    time \

# These build but provide no useful functionality
# as they depend on incomplete or stub features
SBASE_NOT_USEFUL := \
    chgrp \
    chmod \
    chown \
    flock \
    kill \
    ln \
    logger \
    logname \
    mkfifo \
    nice \
    nohup \
    renice \
    setsid \
    whoami \

SBASE_PROGRAMS := \
    basename \
    cal \
    cat \
    cksum \
    cmp \
    cols \
    comm \
    cp \
    cron \
    cut \
    date \
    dirname \
    du \
    echo \
    ed \
    env \
    expand \
    expr \
    false \
    find \
    fold \
    grep \
    head \
    hostname \
    join \
    link \
    ls \
    md5sum \
    mkdir \
    mktemp \
    mv \
    nl \
    od \
    paste \
    pathchk \
    printenv \
    printf \
    pwd \
    readlink \
    rev \
    rm \
    rmdir \
    sed \
    seq \
    sha1sum \
    sha224sum \
    sha256sum \
    sha384sum \
    sha512-224sum \
    sha512-256sum \
    sha512sum \
    sleep \
    sort \
    split \
    sponge \
    strings \
    sync \
    tail \
    tar \
    tee \
    test \
    tftp \
    touch \
    tr \
    true \
    tsort \
    tty \
    uname \
    unexpand \
    uniq \
    unlink \
    uudecode \
    uuencode \
    wc \
    which \
    xinstall \
    yes \

THIRD_PARTY_SBASE_LIBS := system/ulib/fdio system/ulib/c

THIRD_PARTY_SBASE_CFLAGS := \
    -Wno-sign-compare \
    -Wno-missing-field-initializers \
    -Wno-discarded-qualifiers \
    -Wno-maybe-uninitialized \
    -Wno-strict-prototypes \
    -Wno-type-limits

define make-sbase-program
$(eval MODULE := $(THIRD_PARTY_SBASE_DIR).$(1))\
$(eval MODULE_NAME := $(1))\
$(eval MODULE_TYPE := userapp)\
$(eval MODULE_GROUP := misc)\
$(eval MODULE_SRCS += $(THIRD_PARTY_SBASE_DIR)/$(1).c)\
$(eval MODULE_STATIC_LIBS := $(THIRD_PARTY_SBASE_DIR)/libutil $(THIRD_PARTY_SBASE_DIR)/libutf)\
$(eval MODULE_LIBS := $(THIRD_PARTY_SBASE_LIBS))\
$(eval MODULE_CFLAGS := $(THIRD_PARTY_SBASE_CFLAGS))\
$(eval include make/module.mk)
endef

$(foreach pgm,$(SBASE_PROGRAMS),$(call make-sbase-program,$(pgm)))

THIRD_PARTY_SBASE_LIBS := system/ulib/zircon system/ulib/fdio system/ulib/c

$(call make-sbase-program,xargs)
