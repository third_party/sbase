# Copyright 2016 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

MODULE_TYPE := userlib

MODULE_SRCS += \
    $(LOCAL_DIR)/concat.c \
    $(LOCAL_DIR)/cp.c \
    $(LOCAL_DIR)/crypt.c \
    $(LOCAL_DIR)/ealloc.c \
    $(LOCAL_DIR)/enmasse.c \
    $(LOCAL_DIR)/eprintf.c \
    $(LOCAL_DIR)/eregcomp.c \
    $(LOCAL_DIR)/estrtod.c \
    $(LOCAL_DIR)/fnck.c \
    $(LOCAL_DIR)/fshut.c \
    $(LOCAL_DIR)/getlines.c \
    $(LOCAL_DIR)/human.c \
    $(LOCAL_DIR)/linecmp.c \
    $(LOCAL_DIR)/md5.c \
    $(LOCAL_DIR)/memmem.c \
    $(LOCAL_DIR)/mkdirp.c \
    $(LOCAL_DIR)/mode.c \
    $(LOCAL_DIR)/parseoffset.c \
    $(LOCAL_DIR)/putword.c \
    $(LOCAL_DIR)/reallocarray.c \
    $(LOCAL_DIR)/recurse.c \
    $(LOCAL_DIR)/rm.c \
    $(LOCAL_DIR)/sha1.c \
    $(LOCAL_DIR)/sha224.c \
    $(LOCAL_DIR)/sha256.c \
    $(LOCAL_DIR)/sha384.c \
    $(LOCAL_DIR)/sha512-224.c \
    $(LOCAL_DIR)/sha512-256.c \
    $(LOCAL_DIR)/sha512.c \
    $(LOCAL_DIR)/strcasestr.c \
    $(LOCAL_DIR)/strlcat.c \
    $(LOCAL_DIR)/strlcpy.c \
    $(LOCAL_DIR)/strsep.c \
    $(LOCAL_DIR)/strtonum.c \
    $(LOCAL_DIR)/unescape.c \

MODULE_LIBS += system/ulib/c

MODULE_CFLAGS := \
    -Wno-sign-compare -Wno-maybe-uninitialized \
    -Wno-discarded-qualifiers

include make/module.mk
